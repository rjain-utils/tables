
(defmethod initialize-instance :after ((self b+-tree-table) &key)
  (setf (slot-value self 'root)
        (make-array (slot-value self 'node-size)
                    :fill-pointer 0)))

(defun b+-tree-find-node (root key)
  (declare (type simple-vector root))
  )

(defmethod (setf table-find) (new-value (table b+-tree-table) key)
  )

(defmethod table-find ((table b+-tree-table) key)
  )

(defmethod table-remove ((table b+-tree-table) key)
  )
