(in-package :rjain.tables)

(defmethod table-p ((object hash-table))
  t)

(defmethod table-reference-complexity ((object hash-table))
  :linear)

(defmethod table-get ((table hash-table) key)
  (gethash key table))

(defmethod table-set (new-value (table hash-table) key)
  (setf (gethash key table) new-value))

(defmethod table-remove ((table hash-table) key)
  (remhash key table))

(defmethod table-clear ((table hash-table))
  (clrhash table))

