                    
(defmacro define-rb-node-accessor (name rb-node-func null-value)
  `(progn
     (declaim (inline ,name))
     (defun ,name (.node.)
       (etypecase .node.
         (rb-node (,rb-node-func rb-node))
         (null ,null-value)))
     (defsetf ,name #'(setf ,rb-node-func))))

(define-rb-node-accessor rb-node-left %rb-node-left nil)
(define-rb-node-accessor rb-node-parent %rb-node-parent nil)
(define-rb-node-accessor rb-node-key %rb-node-key nil)
(define-rb-node-accessor rb-node-value %rb-node-value nil)
(define-rb-node-accessor rb-node-color %rb-node-color :black)
(define-rb-node-accessor rb-node-size %rb-node-size 0)
(define-rb-node-accessor rb-node-rank %rb-node-rank 0)
(define-rb-node-accessor rb-node-right %rb-node-right nil)

(declaim (inline rb-node-parent-parent))
(defun rb-node-parent-parent (node)
  (rb-node-parent (rb-node-parent node)))

;;; Code mostly borrowed/ripped/stolen from Macro Anotoniotti's
;;; implementation, available in the CMU AI repo.

;;; license agreement from that code:
;;;============================================================================
;;; General License Agreement and Lack of Warranty
;;;
;;; This software is distributed in the hope that it will be useful (both
;;; in and of itself and as an example of lisp programming), but WITHOUT
;;; ANY WARRANTY. The author(s) do not accept responsibility to anyone for
;;; the consequences of using it or for whether it serves any particular
;;; purpose or works at all. No warranty is made about the software or its
;;; performance. 
;;; 
;;; Use and copying of this software and the preparation of derivative
;;; works based on this software are permitted, so long as the following
;;; conditions are met:
;;; 	o  The copyright notice and this entire notice are included intact
;;; 	   and prominently carried on all copies and supporting documentation.
;;; 	o  No fees or compensation are charged for use, copies, or
;;; 	   access to this software. You may charge a nominal
;;; 	   distribution fee for the physical act of transferring a
;;; 	   copy, but you may not charge for the program itself. 
;;; 	o  If you modify this software, you must cause the modified
;;; 	   file(s) to carry prominent notices (a Change Log)
;;; 	   describing the changes, who made the changes, and the date
;;; 	   of those changes.
;;; 	o  Any work distributed or published that in whole or in part
;;; 	   contains or is a derivative of this software or any part 
;;; 	   thereof is subject to the terms of this agreement. The 
;;; 	   aggregation of another unrelated program with this software
;;; 	   or its derivative on a volume of storage or distribution
;;; 	   medium does not bring the other program under the scope
;;; 	   of these terms.
;;; 	o  Permission is granted to manufacturers and distributors of
;;; 	   lisp compilers and interpreters to include this software
;;; 	   with their distribution. 
;;; 
;;; This software is made available AS IS, and is distributed without 
;;; warranty of any kind, either expressed or implied.
;;; 
;;; In no event will the author(s) or their institutions be liable to you
;;; for damages, including lost profits, lost monies, or other special,
;;; incidental or consequential damages arising out of or in connection
;;; with the use or inability to use (including but not limited to loss of
;;; data or data being rendered inaccurate or losses sustained by third
;;; parties or a failure of the program to operate as documented) the 
;;; program, even if you have been advised of the possibility of such
;;; damanges, or for any claim by any other party, whether in an action of
;;; contract, negligence, or other tortious action.
;;;
;;;
;;; The current version of this software and a variety of related
;;; utilities may be obtained by anonymous ftp from ftp.cs.cmu.edu
;;; (128.2.206.173) or any other CS machine in the directory 
;;;       /afs/cs.cmu.edu/user/mkant/Public/Lisp/
;;; You must cd to this directory in one fell swoop, as the CMU
;;; security mechanisms prevent access to other directories from an
;;; anonymous ftp. For users accessing the directory via an anonymous
;;; ftp mail server, the file README contains a current listing and
;;; description of the files in the directory. The file UPDATES describes
;;; recent updates to the released versions of the software in the directory.
;;; The file COPYING contains the current copy of this license agreement.
;;; Of course, if your site runs the Andrew File System and you have
;;; afs access, you can just cd to the directory and copy the files directly.
;;; 
;;; If you wish to be added to the CL-Utilities@cs.cmu.edu mailing list, 
;;; send email to CL-Utilities-Request@cs.cmu.edu with your name, email
;;; address, and affiliation. This mailing list is primarily for
;;; notification about major updates, bug fixes, and additions to the lisp
;;; utilities collection. The mailing list is intended to have low traffic.


#.(let ((names '(right-child-p left-child-p))
        (sides '(rb-node-right rb-node-left)))
    `(progn
       ,(loop
            for name in names
            for side in sides
            collect
              `(defun ,name (node)
                 (eq node (,side (rb-node-parent node)))))))

#.(let ((names '(left-rotate right-rotate))
        (rotate-sides '(rb-node-left rb-node-right))
        (other-sides '(rb-node-right rb-node-left)))
    `(progn
       ,(loop
            for name in names
            for rotate-side in rotate-sides
            for other-side in other-sides
            collect
              `(defun ,name (tree node)
                 (let ((new-parent (,other-side node)))
                   (setf (,other-side node) (,rotate-side new-parent))
                   (when (,rotate-side new-parent)
                     (setf (rb-node-parent (,rotate-side new-parent))
                           node))
                   (setf (rb-node-parent new-parent) (rb-node-parent node))
                   (cond ((not (rb-node-parent node)) ; node is the root
                          (setf (slot-value tree 'root) new-parent))
                         ((left-child-p node)
                          (setf (rb-node-left (rb-node-parent node))
                                new-parent))
                         (t
                          (setf (rb-node-right (rb-node-parent node))
                                new-parent)))
                   ;;; now put the node under its former child
                   (setf (,rotate-side right) node)
                   (setf (rb-node-parent node) right)
                   (update-sizes new-parent node))))))

(defun update-sizes (up-node down-node)
  (setf (rb-node-size up-node) (rb-node-size down-node))
  (setf (rb-node-size down-node)
        (+ 1
           (rb-node-size (rb-node-left down-node))
           (rb-node-size (rb-node-right down-node)))))

(defmethod table-get ((table rb-tree-table) key)
  (loop
      with ord-pred = (table-ordering-predicate table)
      with eq-pred = (table-test table)
      for node = (slot-value table 'root) then
        (if (funcall ord-pred key node-key)
            (rb-node-left node)
            (rb-node-right node))
      for node-key = (rb-node-key node)
      when (null node) then (return (values nil nil))
      when (funcall eq-test key node-key) then
        (return (values (rb-node-value node) t))))

(defmethod table-get/saved-state ((table rb-tree-table) key)
  (loop
      with ord-pred = (table-ordering-predicate table)
      with eq-pred = (table-test table)
      for parent-node = nil then child-node
      for leftp = (funcall ord-pred key node-key)
      for child-node = (slot-value table 'root) then
        (if leftp
            (rb-node-left parent-node)
            (rb-node-right parent-node))
      for node-key = (rb-node-key child-node)
      when (null node) then
        (return (values nil
                        nil
                        (lambda (new-value)
                          (let ((new-node (make-rb-node key new-value)))
                            (if leftp
                                (setf (rb-node-left parent-node)
                                      new-node)
                                (setf (rb-node-right parent-node)
                                      new-node))
                            (rebalance-rb-tree new-node table)))))
      when (funcall eq-test key node-key) then
        (return (values (rb-node-value node)
                        t
                        (lambda (new-value)
                          (setf (rb-node-value node) new-value))))))

(defun rebalance-rb-tree (new-node tree)
  (setf (rb-node-color new-node) :red)
  (loop
      with x = new-node
      until (or (eq x (slot-value table 'root))
                (eq :black (rb-node-parent x)))
      do (macrolet
             ((do-rebalance
                  (same-side other-side other-side-p
                   rotate-same rotate-other)
                `(let ((cousin (,other-side (rb-node-parent-parent x))))
                   (if (eq (rb-node-color y) :red)
                       (setf (rb-node-color (rb-node-parent x)) :black
                             (rb-node-color cousin) :black
                             (rb-node-color (rb-node-parent-parent x))
                             :red)
                       (progn
                         (when (,other-side-p x)
                           (setf x (rb-node-parent x))
                           (,rotate-same x))
                         (setf (rb-node-color (rb-node-parent x)) :black
                               (rb-node-color (rb-node-parent-parent x))
                               :red)
                         (,rotate-other tree
                                        (rb-node-parent-parent x)))))))
           (if (left-child-p (rb-node-parent x))
               (do-rebalance rb-node-left rb-node-right rb-node-right-p
                             rotate-left rotate-right)
               (do-rebalance rb-node-right rb-node-left rb-node-left-p
                             rotate-right rotate-left))))
  (setf (rb-node-color (slot-value table 'root)) :black))

(defmethod table-delete ((table rb-tree-table) key)
  (if (null (slot-value table 'root))
      nil
      (let ((delendum )))))
