(in-package :rjain.tables)

(define-condition table-index-miss (error)
  ((table :initarg :table
          :reader table-index-miss-table)
   (index :initarg :index
          :reader table-index-miss-index))
  (:report (lambda (condition stream)
             (format stream "Index ~A is not in table ~A"
                     (table-index-miss-index condition)
                     (table-index-miss-table condition)))))

(define-condition table-improper-mutation (error)
  ((table :initarg :table
          :reader table-improper-mutation-table)
   (type :initarg type
         :reader table-improper-mutation-type))
  (:report (lambda (condition stream)
             (format stream "Improper mutation ~A on table ~A"
                     (table-improper-mutation-type condition)
                     (table-improper-mutation-table condition)))))

(define-condition hashed-table-collision (error)
  ((table :initarg :table
          :reader hashed-table-collision-table)
   (value :initarg :value
          :reader hashed-table-collision-value)
   (key :initarg :key
        :reader hashed-table-collision-key))
  (:report (lambda (condition stream)
             (format stream "Hashing collision on table ~A, key ~A, ~
existing value ~A"
                     (hashed-table-collision-table condition)
                     (hashed-table-collision-key condition)
                     (hashed-table-collision-value condition)))))

