(in-package :rjain.tables)

(defclass hashed-table (table)
  ((hashing-function :initform 'sxhash
                     :initarg :hashing-function
                     :reader hashed-table-hashing-function)
   (modulus :initform #.most-positive-fixnum
            :initarg :modulus
            :accessor hashed-table-modulus)))

(defclass chaining-collision-resolver ()
  ((rehash-size :initform 1.5
                :initarg :rehash-size
                :accessor table-rehash-size)
   (rehash-threshold :initform 1
                     :initarg :rehash-threshold
                     :accessor table-rehash-threshold)))

(defclass standard-hash-table
    (array-table hashed-table chaining-collision-resolver)
  ((size :initform 7)
   (initial-element :initform nil)))
