(in-package :rjain.tables)

(defclass table ()
  ((test :initform 'eql
         :initarg :test
         :reader table-test)
   (key-type :initform t
             :initarg :key-type
             :reader table-key-type)
   (value-type :initform t
               :initarg :value-type
               :reader table-value-type)))

(defclass ordered-table (table)
  ((ordering-predicate :initarg :ordering-predicate
                       :reader table-ordering-predicate
                       :documentation "Takes a and b and returns t if a is before b")))

(defclass contiguous-table (ordered-table)
  ((min-index :initarg :min-index
              :reader table-min-index)
   (size :initarg :size
         :reader table-size)
   (default-element :initarg :default-element
     :initform nil
     :accessor table-default-element)
   (adjustable-p :initarg :adjustable-p
                 :initform t
                 :reader table-adjustable-p)))
