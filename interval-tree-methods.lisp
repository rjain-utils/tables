(in-package :rjain.tables)

(defmethod table-reference-complexity ((table interval-tree-table))
  :log)

(defmethod shared-initialize :after
           ((table interval-tree-table) slots &key max-index)
  (declare (ignore slots))
  (when max-index (setf (slot-value table 'size) (1+ max-index)))
  (setf (slot-value table 'data)
        (make-array (table-size table)
                    :initial-element (table-initial-element table)
                    :element-type (table-value-type table)
                    :adjustable (table-adjustable-p table))))

(defmethod table-get ((table array-table) key)
  (if (and (fixnum key) (<= 0 key (1- (table-size table))))
      (values (aref (slot-value table 'data) key) t)
      (values nil nil)))

(defun array-table-set (new-value table adjp data key)
  (if adjp
      (progn
        (adjust-array data (1+ key))
        (setf (aref data key) new-value))
      (error 'table-index-miss :table table :index key)))

(defmethod table-get/saved-state ((table array-table) key)
  (with-slots (data adjustable-p) table
    (if (<= 0 key (1- (table-size table)))
        (values (aref (slot-value table 'data) key)
                t
                (lambda (new-value)
                  (setf (aref (slot-value table 'data) key) new-value)))
        (values nil
                nil
                (lambda (new-value)
                  (array-table-set new-value table adjustable-p data key))))))

(defmethod table-set (new-value (table array-table) key)
  (with-slots (data adjustable-p) table
    (array-table-set new-value table adjustable-p data key)))

(defmethod table-iterator-function ((table array-table))
  (let ((index -1)
        (max-index (1- (table-size table)))
        (data (slot-value table 'data)))
    (lambda ()
      (if (= (incf index) max-index)
          (values index (aref data index) nil)
          (values index (aref data index) t)))))
