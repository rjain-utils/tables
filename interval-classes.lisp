(in-package :rjain.tables)

(defclass interval-table (contiguous-table)
  ((key-type :allocation :class
             :initform 'fixnum)
   (ordering-predicate :allocation :class
                       :initform '<)
   (initial-element :initform nil
                    :accessor table-initial-element)
   (data)))
