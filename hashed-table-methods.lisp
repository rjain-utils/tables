(in-package :rjain.tables)

(defgeneric hashed-bucket-get (table bucket hash state key)
  (:documentation
   "Should specialize based on the collision-resolver class. Returns two
values. Initial state is NIL. First is the result, second is T if the
result is the table entry, NIL if the search is a miss, or anything else if
this function is to be called again with the first value as the new hash
and the second value as the new state."))

(defmethod hashed-bucket-get ((table chaining-collision-resolver)
                              bucket hash state key)
  (declare (ignore hash state))
  (let ((result (assoc key bucket :test (table-test table))))
    (if result
        (values (cdr result) t)
        (values nil nil))))

(defgeneric hashed-bucket-get/saved-state (table bucket bucket-setter
                                           hash state key)
  (:documentation
   "Like HASHED-BUCKET-GET but returns one further value which holds the
saved state of the search in order to optimize setting with
HASHED-BUCKET-SET/SAVED-STATE."))

(defmethod hashed-bucket-get/saved-state
    ((table chaining-collision-resolver)
     bucket bucket-setter hash state key)
  (declare (ignore hash state))
  (let ((result (assoc key bucket :test (table-test table))))
    (if result
        (values (cdr result) t (lambda (new-value)
                                 (setf (cdr result) new-value)))
        (values nil nil (lambda (new-value)
                          (funcall bucket-setter (cons key new-value)))))))

(defmethod table-get :around ((table hashed-table) key)
  (let* ((hash (funcall (hashed-table-hashing-function table) key))
         (moded-hash (mod hash (hashed-table-modulus table))))
    (loop
        with value = moded-hash
        and state = nil
        do (multiple-value-setq (value state)
             (hashed-bucket-get
              table (call-next-method table value) value state key))
        until (or (null state) (eq t state))
        finally (return (values value state)))))

(defmethod table-get/saved-state :around ((table hashed-table) key)
  (let* ((hash (funcall (hashed-table-hashing-function table) key))
         (moded-hash (mod hash (hashed-table-modulus table))))
    (loop
        with bucket = nil
        and bucketp = nil
        and bucket-setter = nil
        and value-setter = nil
        and value = moded-hash          ; ends up as result
        and state = nil                 ; ends up as foundp
        do (multiple-value-setq (bucket bucketp bucket-setter)
             (call-next-method table value))
        do (multiple-value-setq (value state value-setter)
             (hashed-bucket-get/saved-state
              table bucket bucket-setter value state key))
        until (or (null state) (eq t state))
        finally (return (values value state value-setter)))))

