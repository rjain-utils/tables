
(deftype color () '(member :red :black))

(defstruct (rb-node
            (:conc-name #:%rb-node)
            (:constructor make-rb-node (key value)))
  (left nil :type '(or null rb-node))
  (parent nil :type '(or null rb-node))
  (key nil)
  (value nil)
  (color :black :type 'color)
  (right nil :type '(or null rb-node)))

(defclass rb-tree-table (ordered-table)
  (root :initform nil))
