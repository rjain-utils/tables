(in-package :rjain.tables)

(defmethod table-reference-complexity ((table alist-table))
  :linear)

(defmethod table-get ((table alist-table) key)
  (let ((result (assoc key (slot-value table 'data)
                       :test (table-test table))))
    (if result
        (values (cdr result) t)
        (values nil nil))))

(defmethod table-get/saved-state ((table alist-table) key)
  (let ((result (assoc key (slot-value table 'data)
                       :test (table-test table))))
    (if result
        (values (cdr result) t result)
        (values nil nil nil))))

(defmethod table-set/saved-state
    (new-value (table alist-table) foundp state key)
  (if foundp
      (setf (cdr state) new-value)
      (push (cons key new-value) (slot-value table 'data))))

(defmethod table-remove ((table alist-table) key)
  (setf (slot-value table 'data)
        (delete key (slot-value table 'data)
                :test (table-test table)
                :key #'car)))

(defmethod table-clear ((table alist-table))
  (setf (slot-value table 'data) '()))

(defmethod table-iterator-function ((table alist-table))
  (let ((list-remainder (slot-value table 'data)))
    #'(lambda ()
        (let ((cell (first list-remainder)))
          (setf list-remainder (rest list-remainder))
          (values (car cell) (cdr cell) (not (null list-remainder)))))))
