(in-package :rjain.tables)

(defclass array-table (contiguous-table)
  ((key-type :allocation :class
             :initform 'fixnum)
   (min-index :allocation :class
              :initform 0)
   (ordering-predicate :allocation :class
                       :initform '<)
   (initial-element :initform nil
                    :accessor table-initial-element)
   (data)))
