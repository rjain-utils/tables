(in-package :rjain.tables)

;;;; TABLE INFORMATIONAL FUNCTIONS

(defgeneric table-p (object)
  (:documentation
   "Returns T if the object supports the table protocol, NIL otherwise.")
  (:method ((object t))
           nil)
  (:method ((object table))
           t))

(defgeneric ordered-table-p (object)
  (:documentation
   "Returns T if the object supports the table protocol and if the table
keeps the associations sorted according to an arbitrary ordering predicate,
NIL otherwise.")
  (:method ((object t))
           nil)
  (:method ((object ordered-table))
           t))

(defgeneric contiguous-table-p (object)
  (:documentation
   "Returns T if the object supports the table protocol and if the table
only stores contiguous ranges of keys, NIL otherwise. Implies
ORDERED-TABLE-P.")
  (:method ((object t))
           nil)
  (:method ((object contiguous-table))
           t))

(defgeneric table-reference-complexity (table)
  (:documentation
   "
Returns ... if complexity is
:CONSTANT   \Theta(1)
:LOG        \omega(1) and o(n)
:LINEAR     \Theta(n)"))

(defgeneric table-equal (table1 table2 &key (test 'eql))
  (:documentation
   "Determines whether the two tables are equal in their type, size,
key-equality predicate, and the mappings from keys to values. The given
TEST is used to determine whether two values in the tables are equal.")
  (:method ((table1 t) (table2 t) &rest rest)
           (declare (ignore table1 table2 rest))
           nil))

;;;; TABLE ACCESSORS AND MUTATORS

(defgeneric table-get (table key)
  (:documentation
   "Tries to find the value assigned to the given key in the given
table. If found, returns the value and T, otherwise returns NIL and NIL."))

(defgeneric table-set (new-value table key)
  (:documentation
   "Sets the value in the table "))

(defgeneric table-get/saved-state (table key)
  (:documentation
   "Like TABLE-GET but returns one further value which holds the saved
state of the search in order to optimize setting with
TABLE-SET/SAVED-STATE.")
  (:method (table key)
           (table-get table key)))

(define-setf-expander table-get (table key)
  (let (($table (gensym "TABLE-"))
        ($key (gensym "KEY-"))
        ($store (gensym "STORE-"))
        ($setter (gensym "SETTER-")))
    (values
     (list $table $key $setter)
     (list table key nil)
     (list $store)
     `(if ,$setter
          (funcall ,$setter ,$store)
          (table-set ,$store ,$table ,$key))
     `(multiple-value-bind (.result. .foundp. .setter.)
          (table-get/saved-state ,$table ,$key)
        (setf ,$setter .setter.)
        (values .result. .foundp.)))))

(declaim (inline table-get/default))
(defun table-get/default (default table key)
  (multiple-value-bind (value foundp) (table-get table key)
    (if foundp value default)))

(define-setf-expander table-get/default (default table key)
  (let (($default (gensym "DEFAULT-"))
        ($table (gensym "TABLE-"))
        ($key (gensym "KEY-"))
        ($store (gensym "STORE-"))
        ($setter (gensym "SETTER-")))
    (values
     (list $default $table $key $setter)
     (list default table key nil)
     (list $store)
     `(if ,$setter
          (funcall ,$setter ,$store)
          (table-set ,$store ,$table ,$key))
     `(multiple-value-bind (.result. .foundp. .setter.)
          (table-get/saved-state ,$table ,$key)
        (setf ,$setter .setter.)
        (if .foundp. .result. ,$default)))))

(declaim (inline table-get/error))
(defun table-get/error (table key)
  (multiple-value-bind (value foundp) (table-get table key)
    (if foundp value (error 'table-index-miss :table table :index key))))

(define-setf-expander table-get/error (table key)
  (let (($table (gensym "TABLE-"))
        ($key (gensym "KEY-"))
        ($store (gensym "STORE-"))
        ($setter (gensym "SETTER-")))
    (values
     (list $table $key $setter)
     (list table key nil)
     (list $store)
     `(if ,$setter
          (funcall ,$setter ,$store)
          (table-set ,$store ,$table ,$key))
     `(multiple-value-bind (.result. .foundp. .setter.)
          (table-get/saved-state ,$table ,$key)
        (setf ,$setter .setter.)
        (if .foundp.
            .result.
            (error 'table-index-miss :table ,$table :index ,$key))))))

(defgeneric table-remove (table key)
  (:documentation
   "Removes the given set of keys from the table's list of valid
associations. Returns T if the assocation was found, NIL if not."))

(defgeneric table-clear (table)
  (:documentation
   "Clears all key-value assocations in the table."))

(defgeneric table-iterator-function (table)
  (:documentation
   "Returns a function of no arguments which will iterate over all the
associations in the table, returning the key, the value, and a boolean
indicating whether there are more items remaining in the iteration."))

(declaim (inline call-mapping-table))
(defun call-mapping-table (table function)
  "Takes a table and a function of two arguments. For each key, value pair
in the table, the function is called with they key and value as arguments."
  (let ((iterator (table-iterator-function table)))
    (loop
        with key = nil and value = nil and morep = nil
        do (multiple-value-setq (key value morep) (funcall iterator))
        while morep
        do (funcall function key value))))

(defmacro map-table ((table key value) &body body)
  "Executes the body for each association in the table, binding key to the
key in that association, and value to the value."
  `(call-mapping-table ,table #'(lambda (,key ,value) ,@body)))

