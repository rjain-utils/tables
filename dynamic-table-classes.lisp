
(defclass dynamic-table ()
  ())

(defclass dynamic-contiguous-table (dynamic-table contiguous-table)
  ((run-frequency :initarg :run-frequency
                  :initform :low
                  :accessor table-run-frequency)))
