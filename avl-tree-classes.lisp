    
(defstruct (avl-tree-node (:conc-name node))
  (min)
  (left)
  (key)
  (value)
  (right)
  (max))

(defclass avl-tree-table (ordered-table)
  (root)
  (balance :initarg :balance
           :initform :balanced
           :type '(member :balanced :left :right)))
