(in-package :rjain.tables)

(defclass list-table (contiguous-table)
  ((min-index :allocation :class
              :initform 0)
   (ordering-predicate :allocation :class
                       :initform '<)
   (data :initform '())))
