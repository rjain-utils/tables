(in-package :rjain.tables)

(defmethod table-reference-complexity ((table list-table))
  :linear)

(defmethod table-get ((table list-table) key)
  (declare (type fixnum key))
  (do ((list-remainder (slot-value table 'data) (cdr list-remainder))
       (index key (1- index)))
      ((or (zerop index) (null list-remainder))
       (if (null list-remainder)
           (values nil nil)
           (values (car list-remainder) t)))
    (declare (type fixnum index)
             (type list list-remainder))))

(defmethod table-get/saved-state ((table list-table) key)
  (declare (type fixnum key))
  (do ((prev-remainder nil list-remainder)
       (list-remainder (slot-value table 'data) (cdr list-remainder))
       (index key (1- index)))
      ((or (zerop index) (null list-remainder))
       (if (null list-remainder)
           (values nil
                   nil
                   (lambda (new-value)
                     (if (table-adjustable-p table)
                         (loop
                             for i from (table-size table) to key
                             for cell = prev-remainder then (cdr cell)
                             do (setf (cdr cell) (cons nil nil))
                             finally (setf (car cell) new-value))
                         (error 'table-index-miss
                                :table table :index key))))
           (values (car list-remainder)
                   t
                   (lambda (new-value)
                     (setf (car list-remainder) new-value)))))
    (declare (type fixnum index)
             (type list list-remainder))))

(defmethod table-set (new-value (table list-table) key)
  (declare (type fixnum key))
  (with-slots (data default-element adjustable-p) table
    (when (null data)
      (setf data (list default-element)))
    (loop
        for i fixnum from 0 to key
        for cell of-type cons = data then (cdr cell)
        when (and (null (cdr cell)) (> key i))
        do (if adjustable-p
               (setf (cdr cell) (list default-element))
               (error 'table-index-miss :table table :index key))
        finally (setf (car cell) new-value))
    new-value))

(defmethod table-iterator-function ((table list-table))
  (let ((index -1)
        (cell (slot-value table 'data)))
    (lambda ()
      (if (null (cdr cell))
          (values (incf index) (car cell) nil)
          (values (incf index) (car cell) t)))))
